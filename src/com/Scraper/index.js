const moment = require('moment'),
  Save = require('../Save');

class Scraper {

  /*
  * Method to be overriden
  * @param {Page} page - Page object (puppeteer)
  * @param {number} index - index in start_urls
  * @param {this} self - reference to self
  */
  scrape_page(page, index, self){
    return new Promise((resolve)=>resolve(true));
  }

  nextPage(page, index, page_number){
    return new Promise((resolve)=>resolve(true));
  }

  beforeScrape(page,index){
    return new Promise((resolve)=>resolve(true));
  }

  /*
  * @param {number} number_pages - the number of pages to scrape
  * @param {string} url - the url of page to start on
  */
  async startScrape(page, number_pages = 3, url) {
    if( url ){
      this.start_urls = [url];
    }

    for( let i=0; i<this.start_urls.length; i++ ){
      await page.goto(this.start_urls[i],{waitUntil: 'networkidle0'});
      await this.beforeScrape(page,i);
      await page.waitFor(10000);

      for( let j=0; j<number_pages; j++ ){
        await this.scrape_page(page, i, this);
        await this.nextPage(page,i,j);
        await page.waitFor(10000);
      }
    }
  }

  /**
   * Converts given date to Javascript Date
   * @param {string} date  - date to be formatted
   * @param {string} format - input date format (default YYYY-MM-DD)
   * @returns {Date} - Javascript Date object
   */
  date(date, format) {
     if( format ){
       return moment(date,format).utc().toDate();
     }
     else {
       return moment(date,'YYYY-MM-DD').utc().toDate();
     }
  }

  async saveScene(scene, exit=false){
    let keys = Object.keys(scene);
    for( let i=0; i<keys.length; i++ ){
      let key = keys[i];
      if( scene[key] === null ){
        delete scene[key];
      }

      if( scene[key] instanceof Object ){
        await this.saveScene(scene[key], true);
      }
    }

    if( exit ){
      return;
    }

    await Save(scene);
  }

  newScene(){
    return {
      network: this.network,
      site: null,
      date: null,
      type: null,
      dvd: {
        title: null,
        series: null,
        volume: null,
        volume_title: null,
        scene: null,
        frontCover: null,
        backCover: null,
      },
      title: null,
      actors: [],
      tags: [],
      url: null,
      images: [],
      source_id : null,
      description: null,
    }
  }
}

module.exports = Scraper;
