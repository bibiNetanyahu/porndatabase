const cheerio = require('cheerio'),
  Scraper = require('../com/Scraper');

class scraper extends Scraper {
  constructor(){
    super();
    this.network = 'milehighmedia';
    this.start_urls = ['https://www.milehighmedia.com/en/videos']
    this.base_urls = ['https://www.milehighmedia.com']
  }

  async scrape_page(page, index, self){
    let $ = cheerio.load(await page.content());
    let containers = $('.sceneContainer').toArray();

    for( let i=0; i<containers.length; i++ ){
      let info = $(containers[i]);
      let scene = self.newScene();
      
      scene.site = info.find('.fromSite').find('a').text().replace('.com','').trim();
      scene.date = self.date( info.find('.sceneDate').text() );
      scene.type = 'dvd';

      scene.title = info.find('.sceneTitle').find('a').attr('title');
      scene.url = self.base_urls[index]+info.find('a.imgLink').attr('href');
      scene.source_id = info.find('a.imgLink').attr('data-id');
      scene.actors = info.find('.sceneActors').find('a').map( (i,actor)=>{
        return $(actor).attr('title');
      }).get();
      scene.images = info.find('a.imgLink').find('img').map( (i,img)=>{
        return $(img).attr('src');
      }).get();

      if( ! scene.url ) continue;
 
      await page.goto(scene.url, {waitUntil: 'networkidle0'});
      await self.scrape_scene(page, scene);
      await page.goBack({waitUntil: 'networkidle0'});

      self.saveScene(scene);
    }
  }

  async scrape_scene(page, scene){
    let $ = cheerio.load(await page.content());
    let info = $('#sceneInfo'); 

    scene.dvd.title = info.find('img.sceneImage').attr('title');
    scene.dvd.frontCover = info.find('img').attr('src');
    scene.tags = info.find('.sceneColCategories').find('a').map( (i,tag)=>{
      return $(tag).attr('title').toLowerCase();
    }).get();
    scene.description = info.find('.sceneDesc').text().replace('Video Description:','').trim();
  }

  async nextPage(page,index,page_number){
    await page.click('.next');
  }
}

module.exports = scraper;
