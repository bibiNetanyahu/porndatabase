const fs = require('fs');
const puppeteer = require('puppeteer');

(async ()=>{
  let items = await readdir();
  const browser = await puppeteer.launch({
    headless: true,
    args: [
      '--no-sandbox',
      '--disable-setuid-sandbox',
    ]
  });
  const page = await browser.newPage();
  await page.setViewport({
    width: 1920,
    height: 1080
  });

  for( let i=0; i<items.length; i++ ){
    let site = items[i];
    if( ! site.endsWith('.js') ){
      continue
    }

    let scraper = require( `${__dirname}/sites/${site}` );
    let obj = new scraper();
    await obj.startScrape(page);  
  }

  await browser.close();
})();

function readdir(){
  return new Promise((resolve,reject)=>{
    fs.readdir( `${__dirname}/sites`, function(err,items){
      if(err) return reject(err);
      resolve(items);
    });
  });
}
